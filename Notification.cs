namespace projectNotification
{
    public class Notification
    {
        public string Code { get; }

        public string Message { get; }

        public Notification(string code, string message)
        {
            this.Code = code;
            this.Message = message;
        }

        public override string ToString() => Message;
    }
}