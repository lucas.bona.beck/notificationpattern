namespace projectNotification
{
    public class User: Notifiable
    {
        public User(string email, string password)
        {
            this.Email = email;
            this.Password = password;

            if(string.IsNullOrEmpty(email))
            {
                AddNotification(new Notification("USU-01", "Email must be informed."));
            }

            if(string.IsNullOrEmpty(password))
            {
                AddNotification(new Notification("USU-02", "Password must be informed."));
            }
        }

        public string Email { get; private set; }
        public string Password { get; private set; }
    }
}