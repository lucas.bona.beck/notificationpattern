using System.Collections.Generic;

namespace projectNotification
{
    public abstract class Notifiable
    {
        private List<Notification> _notifications;

        public IReadOnlyCollection<Notification> Notifications => InternalNotifications;

        private List<Notification> InternalNotifications
        {
            get
            {
                if(_notifications == null)
                {
                    _notifications = new List<Notification>();
                }

                return _notifications;
            }
        }

        public void AddNotification(Notification notification)
        {
            if(notification != null)
            {
                InternalNotifications.Add(notification);
            }
        }

        public void ClearNotification() => _notifications = null;

        public bool IsValid() => InternalNotifications.Count == 0;
    }
}